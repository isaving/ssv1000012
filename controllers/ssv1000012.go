//Version: v0.0.1
package controllers

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/isaving/sv/ssv1000012/models"
	"git.forms.io/isaving/sv/ssv1000012/services"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000012Controller struct {
    controllers.CommController
}

func (*Ssv1000012Controller) ControllerName() string {
	return "Ssv1000012Controller"
}

// @Desc ssv1000012 controller
// @Description Entry
// @Param ssv1000012 body models.SSV1000012I true "body for user content"
// @Success 200 {object} models.SSV1000012O
// @router /ssv1000012 [post]
// @Author
// @Date 2020-12-04
func (c *Ssv1000012Controller) Ssv1000012() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000012Controller.Ssv1000012 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000012I := &models.SSV1000012I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv1000012I); err != nil {
		c.SetServiceError(err)
		return
	}
  if err := ssv1000012I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000012 := &services.Ssv1000012Impl{} 
    ssv1000012.New(c.CommController)
	ssv1000012.Sv100012I = ssv1000012I

	ssv1000012O, err := ssv1000012.Ssv1000012(ssv1000012I)

	if err != nil {
		log.Errorf("Ssv1000012Controller.Ssv1000012 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv1000012O.PackResponse()
	if err != nil {
		c.SetServiceError(err)
		return
	}
	c.SetAppBody(responseBody)
}
// @Title Ssv1000012 Controller
// @Description ssv1000012 controller
// @Param Ssv1000012 body models.SSV1000012I true body for SSV1000012 content
// @Success 200 {object} models.SSV1000012O
// @router /create [post]
/*func (c *Ssv1000012Controller) SWSsv1000012() {
	//Here is to generate API documentation, no need to implement methods
}*/

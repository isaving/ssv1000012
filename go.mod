module git.forms.io/isaving/sv/ssv1000012

go 1.13

require (
	git.forms.io/legobank/legoapp v0.0.0
	git.forms.io/isaving/models v0.0.0
	git.forms.io/universe v0.0.0
	github.com/astaxie/beego v1.12.2
	github.com/golang/mock v1.4.3
	github.com/jung-kurt/gofpdf v1.16.2 // indirect
	github.com/pkg/sftp v1.12.0 // indirect
	github.com/urfave/cli v1.22.4
)

replace (
	git.forms.io/legobank/legoapp v0.0.0 => ../../../../git.forms.io/legobank/legoapp
	git.forms.io/isaving/models v0.0.0 => ../../../../git.forms.io/isaving/models
	git.forms.io/universe v0.0.0 => ../../../../git.forms.io/universe

	github.com/tidwall/gjson => github.com/tidwall/gjson v1.3.6
)

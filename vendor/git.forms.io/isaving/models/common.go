package models

import ("fmt"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	)

type CommonResponse struct {
	ReturnCode string      `json:"returnCode"`
	ReturnMsg  string      `json:"returnMsg"`
	Data       interface{} `json:"data"`
}

const (
	successCode = "0"
	successMsg  = "success"
)

type Float64 float64

//to keep leading zero of the number
func (f Float64) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%f", f)), nil
}

func UnPackRequest(requestBody []byte, request interface{}) error {

	if err := json.Unmarshal(requestBody, request); err != nil {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

func PackRequest(request interface{}) (requestBody []byte, err error) {

	requestBody, err = json.Marshal(request)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}
	return requestBody, nil

}

func PackResponse(response interface{}) (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       response,
	}
	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}
	return responseBody, nil
}

func PackErrorResponse(response interface{}, e interface{}) (responseBody []byte, err error) {

	returnCode := ""
	returnMsg := ""
	switch errIn := e.(type) {
	case *errors.ServiceError:
		returnCode = errors.GetErrorCode(errIn)
		returnMsg = errIn.Error()
	case errors.ServiceError:
		returnCode = errors.GetErrorCode(&errIn)
		returnMsg = errIn.Error()
	default:
		returnCode = constant.INVALIDERR
		returnMsg = fmt.Sprintf("Invalid service error:[%++v]", e)
	}

	commResp := &CommonResponse{
		ReturnCode: returnCode,
		ReturnMsg:  returnMsg,
		Data:       response,
	}
	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}
	return responseBody, nil
}

func UnPackResponse(responseBody []byte, response interface{}) error {
	commResp := &CommonResponse{
		Data: response,
	}
	if err := json.Unmarshal(responseBody, commResp); err != nil {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}
	return nil
}



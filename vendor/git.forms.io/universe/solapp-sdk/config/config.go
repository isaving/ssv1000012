//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package config

import (
	"bufio"
	"fmt"
	"github.com/astaxie/beego/utils"
	"github.com/go-errors/errors"
	"io"
	"net"
	"os"
	"path/filepath"
	"strings"

	constant "git.forms.io/universe/solapp-sdk/const"
	"github.com/astaxie/beego"
)

var CmpSvrConfig CmpServiceConfig

type DeploymentConfig struct {
	IsEnableSecure bool
	Mode           string
}

type CmpServiceConfig struct {
	// 公共配置
	ServerPort       string
	CommAgentAddress string
	CommType         string
	ConnectMode      string

	// service相关配置
	//NodeId     string
	//InstanceId string
	//EnvUniNodeId     string
	//EnvUniInstanceId string
	DcnWithoutFirstByte string
	InstanceID          string

	DtsPropagator bool

	// dts agent相关配置(mesh)
	DtsEnvFilePath         string
	DtsAgentOrganization   string
	DtsAgentDataCenterNode string
	DtsAgentNodeId         string
	DtsAgentInstanceId     string

	DtsAgentRegisterTopicName   string
	DtsAgentEnlistTopicName     string
	DtsAgentTryResultReportName string

	// dts agent相关配置(direct)
	DtsAgentAddress        string
	RegisterUrlPath        string
	EnlistUrlPath          string
	TryResultReportUrlPath string

	// dts client相关配置
	DtsClientConfirmTopicId   string
	DtsClientConfirmTopicName string
	DtsClientConfirmQueueName string

	DtsClientCancelTopicId   string
	DtsClientCancelTopicName string
	DtsClientCancelQueueName string

	DtsTimeoutMilliseconds int
	DtsMaxRetryTimes       int

	// dts client相关配置(direct)
	ConfirmUrlPath     string
	CancelUrlPath      string
	ParticipantAddress string

	HeartbeatTopicName       string
	HeartbeatIntervalSeconds int

	AlertTopicName string

	DBMaxIdleConns      int
	DBMaxOpenConns      int
	DBDefaultQueryLimit int
	DBMaxLimitValue     int
	DBMaxLifeValue      int

	IsIgnoreServiceConfig bool
	DstDcn                string
	DstOrg                string
	AppName               string
	DcnNo                 string
	Langs                 string

	Deployment DeploymentConfig
}

func InitSolappConfig() (err error) {
	//if err = config.InitCommConfig(); nil != err {
	//	log.Errorf("init comm-agent config failed[%v], please check", err)
	//	return err
	//}
	fmt.Println("start init solapp config...")
	bc := beego.AppConfig

	CmpSvrConfig = CmpServiceConfig{
		ServerPort:       bc.String("httpport"),
		CommAgentAddress: bc.String("comm::agent.address"),
		CommType:         bc.DefaultString("comm::type", "mesh"),

		//DtsEnvFilePath:         bc.DefaultString("dts::envFilePath", constant.DEFAULT_DTS_ENV_FILE_PATH),
		DtsAgentOrganization:   bc.String("dts_agent.mesh::organization"),
		DtsAgentDataCenterNode: bc.String("dts_agent.mesh::dataCenterNode"),
		DtsAgentNodeId:         bc.DefaultString("dts_agent.mesh::nodeId", constant.DTS_AGENT_DEFAULT_NODE_ID),
		DtsAgentInstanceId:     bc.String("dts_agent.mesh::instanceId"),

		DtsAgentRegisterTopicName:   bc.String("dts_agent.mesh::topic.registerName"),
		DtsAgentEnlistTopicName:     bc.String("dts_agent.mesh::topic.enlistName"),
		DtsAgentTryResultReportName: bc.String("dts_agent.mesh::topic.tryResultReportName"),
		DtsPropagator:               bc.DefaultBool("dts::isPropagator", false),
		DtsTimeoutMilliseconds:      bc.DefaultInt("dts::timeoutMilliseconds", 5000),
		DtsMaxRetryTimes:            bc.DefaultInt("dts::maxRetryTimes", 3),

		DtsAgentAddress:        bc.String("dts_agent.direct::address"),
		RegisterUrlPath:        bc.DefaultString("dts_agent.direct::registerUrlPath", constant.ROOT_TXN_REGISTER_PATH),
		EnlistUrlPath:          bc.DefaultString("dts_agent.direct::enlistUrlPath", constant.BRANCH_TXN_ENLIST_PATH),
		TryResultReportUrlPath: bc.DefaultString("dts_agent.direct::tryResultReportUrlPath", constant.TXN_TRY_RESULT_REPORT_PATH),

		DtsClientConfirmTopicId: bc.String("dts_client.mesh::topic.confirmName"),
		DtsClientCancelTopicId:  bc.String("dts_client.mesh::topic.cancelName"),

		ConfirmUrlPath:     bc.DefaultString("dts_client.direct::confirmUrlPath", constant.TXN_CALLBACK_CONFIRM),
		CancelUrlPath:      bc.DefaultString("dts_client.direct::cancelUrlPath", constant.TXN_CALLBACK_CANCEL),
		ParticipantAddress: fmt.Sprintf("http://%s:%s", CurrentHost(), bc.String("httpport")),

		HeartbeatTopicName:       bc.String("heartbeat::topicName"),
		HeartbeatIntervalSeconds: bc.DefaultInt("heartbeat::intervalSeconds", 15),

		AlertTopicName: bc.String("alert::topicName"),

		DBMaxIdleConns:      bc.DefaultInt("mysql::maxIdleConns", 10),
		DBMaxOpenConns:      bc.DefaultInt("mysql::maxOpenConns", 10),
		DBDefaultQueryLimit: bc.DefaultInt("mysql::defaultQueryLimit", 30),
		DBMaxLimitValue:     bc.DefaultInt("mysql::maxLimitValue", 50000),
		DBMaxLifeValue:      bc.DefaultInt("mysql::maxLifeValue", 540),
		ConnectMode:         bc.String("connectMode"),

		IsIgnoreServiceConfig: beego.AppConfig.DefaultBool("ignore_service_config", false),
		DstDcn:                beego.AppConfig.String("service::groupDcn"),
		DstOrg:                beego.AppConfig.String("service::organization"),
		AppName:               beego.AppConfig.String("appname"),
		DcnNo:                 beego.AppConfig.String("service::dataCenterNode"),
		Langs:                 beego.AppConfig.String("lang::types"),
	}

	if "" != CmpSvrConfig.DtsAgentOrganization {
		if "" == CmpSvrConfig.DtsClientConfirmTopicId || "" == CmpSvrConfig.DtsClientCancelTopicId {
			panic("Topic of DTS Client(confirm or cancel) cannot be empty!")
		}
		if CmpSvrConfig.DtsClientConfirmTopicId == CmpSvrConfig.DtsClientCancelTopicId {
			panic("Topic of DTS Client(confirm and cancel) cannot be the same!")
		}

		//if cfg, err := InitConfig(CmpSvrConfig.DtsEnvFilePath); nil != err {
		//	log.Errorf("load config file failed, file path:[%s], error:[%v]", CmpSvrConfig.DtsEnvFilePath, err)
		//} else {
		//	log.Infof("Set DTS Agent instance id:[%s]", strings.TrimSpace(cfg[constant.DTS_AGENT_INSTANCE_ID_KEY]))
		//	CmpSvrConfig.DtsAgentInstanceId = strings.TrimSpace(cfg[constant.DTS_AGENT_INSTANCE_ID_KEY])
		//}

		if "" == CmpSvrConfig.DtsAgentInstanceId {
			panic("Cannot found DTS agent instance id, please check!")
		}

		fmt.Printf("DTS agent org:[%s],dcn:[%s],nodeId:[%s], instanceId:[%s]\n", CmpSvrConfig.DtsAgentOrganization,
			CmpSvrConfig.DtsAgentDataCenterNode, CmpSvrConfig.DtsAgentNodeId, CmpSvrConfig.DtsAgentInstanceId)
	}

	if !CmpSvrConfig.IsIgnoreServiceConfig {
		if CmpSvrConfig.DstDcn == "" {
			panic("The groupDcn of service can not be empty")
		}

		if CmpSvrConfig.DstOrg == "" {
			panic("The organization of service can not be empty")
		}

		if CmpSvrConfig.AppName == "" {
			panic("The appname can not be empty")
		}

		if CmpSvrConfig.DcnNo == "" {
			panic("The dataCenterNode of service can not be empty")
		} else {
			CmpSvrConfig.DcnWithoutFirstByte = CmpSvrConfig.DcnNo[1:]
		}
	}

	instanceID := strings.ReplaceAll(strings.ReplaceAll(os.Getenv("INSTANCE_ID"), "-", ""), "_", "")

	if strings.HasPrefix(CmpSvrConfig.DtsAgentNodeId, "$") {
		nodeID := os.Getenv("NODE_ID")
		if "" == nodeID {
			panic("cannot found env key `NODE_ID`")
		} else {
			fmt.Printf("Replace dts_agent.mesh.nodeId with:[%s]\n", nodeID)
			CmpSvrConfig.DtsAgentNodeId = nodeID
		}
	}

	//ins := instance[0]
	//if len(instance) > 1 {
	//	instanceID = ins + fmt.Sprintf("%02v", instance[1])
	//} else {
	//	instanceID = ins + "00"
	//}

	CmpSvrConfig.InstanceID = instanceID
	deployment := &DeploymentConfig{}

	isEnableDeploySecure := beego.AppConfig.DefaultBool("deployment::enableSecure", false)

	deployment.IsEnableSecure = isEnableDeploySecure
	deployment.Mode = beego.AppConfig.DefaultString("deployment::mode", "B")

	CmpSvrConfig.Deployment = *deployment

	if true == isEnableDeploySecure && "C" == deployment.Mode {
		// delete config file
		if filepath, err := GetConfigPath(); nil == err {
			fmt.Println("log file path:", filepath)
			if err := os.Remove(filepath); nil != err {
				panic(err)
			}
		} else {
			panic(err)
		}
	}

	fmt.Println("init solapp config successfully!")
	return
}

func GetConfigPath() (string, error) {
	AppPath, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return "", err
	}
	workPath, err := os.Getwd()
	if err != nil {
		return "", err
	}
	var filename = "app.conf"
	appConfigPath := filepath.Join(workPath, "conf", filename)
	if !utils.FileExists(appConfigPath) {
		appConfigPath = filepath.Join(AppPath, "conf", filename)
		if !utils.FileExists(appConfigPath) {
			return "", errors.New("Cannot found conf/app.conf")
		}
	}

	return appConfigPath, nil
}

func (c CmpServiceConfig) IsConnectModeUnixSocket() bool {
	return "unixSocket" == c.ConnectMode
}

func InitConfig(configFilePath string) (map[string]string, error) {
	fmt.Printf("Loading config file:%s\n", configFilePath)
	//初始化
	myMap := make(map[string]string)

	//打开文件指定目录，返回一个文件f和错误信息
	f, err := os.Open(configFilePath)
	defer f.Close()

	//异常处理 以及确保函数结尾关闭文件流
	if err != nil {
		return nil, err
	}

	//创建一个输出流向该文件的缓冲流*Reader
	r := bufio.NewReader(f)
	for {
		//读取，返回[]byte 单行切片给b
		b, _, err := r.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}

		//去除单行属性两端的空格
		s := strings.TrimSpace(string(b))
		//fmt.Println(s)

		//判断等号=在该行的位置
		index := strings.Index(s, "=")
		if index < 0 {
			continue
		}
		//取得等号左边的key值，判断是否为空
		key := strings.TrimSpace(s[:index])
		if len(key) == 0 {
			continue
		}

		//取得等号右边的value值，判断是否为空
		value := strings.TrimSpace(s[index+1:])
		if len(value) == 0 {
			continue
		}
		//这样就成功吧配置文件里的属性key=value对，成功载入到内存中c对象里
		myMap[key] = value
	}
	return myMap, nil
}

func CurrentHost() (host string) {
	host = "localhost"
	netInterfaces, e := net.Interfaces()
	if e != nil {
		return
	}

	for i := 0; i < len(netInterfaces); i++ {
		if (netInterfaces[i].Flags & net.FlagUp) != 0 {
			addrs, _ := netInterfaces[i].Addrs()

			for _, address := range addrs {
				if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
					if ipnet.IP.To4() != nil {
						host = ipnet.IP.String()
						return
					}
				}
			}
		}
	}

	return
}

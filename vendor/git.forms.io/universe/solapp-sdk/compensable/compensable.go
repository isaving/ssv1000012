//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package compensable

var CmpTxnCtsInstance = &CmpTxnCtx{}

type CmpTxnCtx struct{}

type SpanContext struct {
	TraceId      string
	SpanId       string
	ParentSpanId string
}

type TxnCtx struct {
	DtsAgentAddress string
	RootXid         string
	ParentXid       string
	BranchXid       string
	Level           int
	CurrentTime     string
	LastUpdateTime  string

	SpanCtx *SpanContext
}

func (c *CmpTxnCtx) NewTxnCtx() *TxnCtx {
	return &TxnCtx{}
}

//func (c *CmpTxnCtx) GetContext() (result *TxnCtx) {
//	if ret := util.Get(TxnCtxKey); nil != ret {
//		result = ret.(*TxnCtx)
//	}
//	return
//}

//func (c *CmpTxnCtx) SetContext(ctx *TxnCtx) {
//	util.Set(TxnCtxKey, ctx)
//}

//func (c *CmpTxnCtx) CleanupContext() {
//	util.Cleanup()
//}

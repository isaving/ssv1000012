//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package util

import (
	"fmt"
	"git.forms.io/universe/solapp-sdk/config"
	"strconv"
	"sync"
	"time"
)

var flagNo int64 = 0
var lock sync.Mutex

const SEQMAX = 999999

//func BytesToInt(bys []byte) int {
//	bytebuff := bytes.NewBuffer(bys)
//	var data int8
//	binary.Read(bytebuff, binary.BigEndian, &data)
//	return int(data)
//}
//
//var ZERO_LPAD = []string{
//	"",
//	"0",
//	"00",
//	"000",
//}
//
//var MAX_LEN = 3
//
//func main() {
//	instanceId := "wl0trf01-SERVICE_BROKERID1-0"
//	fmt.Println(strings.ReplaceAll(strings.ReplaceAll(instanceId, "-", ""), "_", ""))
//	instanceBytes := []byte(instanceId)
//	for _, bt := range instanceBytes {
//		s := strconv.Itoa(BytesToInt([]byte{bt}))
//		paddingLength := MAX_LEN - len(s)
//		fmt.Printf("%s", ZERO_LPAD[paddingLength] + s)
//	}
//}

func GenerateSerialNo(tp string) string {
	dcnNo := config.CmpSvrConfig.DcnNo
	instanceID := config.CmpSvrConfig.InstanceID
	timestamp := time.Now().Unix()

	serialNo := tp + dcnNo + instanceID + strconv.FormatInt(timestamp, 10)

	lock.Lock()
	defer lock.Unlock()
	flagNo++
	if flagNo > SEQMAX {
		flagNo = 1
	}
	flagNo2Str := strconv.FormatInt(flagNo, 10)
	flagNo2Str = fmt.Sprintf("%06v", flagNo2Str)
	serialNo = serialNo + flagNo2Str
	return serialNo
}

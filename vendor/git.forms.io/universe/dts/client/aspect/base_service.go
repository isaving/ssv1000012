//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package aspect

import (
	"git.forms.io/universe/solapp-sdk/compensable"
)

// @Desc the DTS's Basic information
type DTSBaseService struct {
	DTSCtx *compensable.TxnCtx
}

package models

type FormHead struct {
	FormId string
}

type Body struct {
	Form []Form
}
type Form struct {
	FormHead FormHead
	FormData map[string]interface{}
}

type BodyArr struct {
	Form []FormArr
}

type FormArr struct {
	FormHead FormHead
	FormData []map[string]interface{}
}

type BodyMult struct {
	Form []FormMult
}

type FormMult struct {
	FormHead FormHead
	FormData interface{}
}

type ParamSub struct {
	ParamGroup string `json:"ParmGroup"`
}

type ParamResp struct {
	Form []struct {
		FormHead struct {
			FormId string
		}
		FormData map[string]string
	}
}

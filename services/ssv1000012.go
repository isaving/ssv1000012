//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/sv/ssv1000012/constant"
	"git.forms.io/isaving/sv/ssv1000012/models"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
)

type Ssv1000012Impl struct {
	services.CommonService
	Sv100012O *models.SSV1000012O
	Sv100012I *models.SSV1000012I
}

// @Desc Ssv1000012 process
// @Author
// @Date 2020-12-04
func (impl *Ssv1000012Impl) Ssv1000012(ssv1000012I *models.SSV1000012I) (ssv1000012O *models.SSV1000012O, err error) {

	impl.Sv100012I = ssv1000012I
	querySCU0000006O, err := impl.querySCU0000006()
	if err != nil {
		return nil, err
	}
	ssv1000012O = &models.SSV1000012O{
		CustStatus: querySCU0000006O.CustStatus, //客户状态
	}
	return ssv1000012O, nil
}

//CU000006   scu0000006
func (impl *Ssv1000012Impl) querySCU0000006() (*models.SCU0000006O, error) {
	scu0000006I := models.SCU0000006I{
		CustId: impl.Sv100012I.CustId, //客户编号
	}
	requestBody, err := models.PackRequest(scu0000006I)
	if err != nil {
		return nil, errors.Errorf(constant.ERRCODE_IL9, "%v", err)
	}
	responseBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constant.CU000006, requestBody)
	if err != nil {
		return nil, errors.Errorf(constant.ERRCODE_IL9, "querySCU0000006 failed : %v", err)
	}

	SCU0000006O := models.SCU0000006O{}
	err = SCU0000006O.UnPackResponse(responseBody)
	if err != nil {
		return nil, err
	}
	return &SCU0000006O, nil
}

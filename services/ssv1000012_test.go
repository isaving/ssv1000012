package services

import (
	"git.forms.io/isaving/sv/ssv1000012/models"
	"testing"
)

var cu000006Response = `{"returnCode":"0","returnMsg":"success","response":{"CustStatus":"1"}}`

func (impl *Ssv1000012Impl) RequestSyncServiceElementKey(
	elementType, elementId, serviceKey string,
	requestData []byte) (responseData []byte, err error) {
	switch serviceKey {
	case "CU000006":
		responseData = []byte(cu000006Response)

	}

	return responseData, nil
}

func TestTryCommService(t *testing.T) {
	impl := &Ssv1000012Impl{
		Sv100012I: &models.SSV1000012I{
			CustId:"1",
		},
	}
	sv100013O, err := impl.Ssv1000012(impl.Sv100012I)
	if err != nil {
		t.Error(err)
	} else {
		t.Log(sv100013O)
	}
}




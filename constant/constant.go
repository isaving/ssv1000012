//Version: v0.0.1
package constant

//define error code
const (
)
const (
	DlsuDcnLists   = "DlsuDcnLists"   // dcnList
	DLSUPCREATEDTS = "DLSUPCREATEDTS" // dts
	DLSUPUPDATEDTS = "DLSUPUPDATEDTS" // dts
	DTS_TOPIC_TYPE = "DTS"            // dts
	TRN_TOPIC_TYPE = "TRN"            // trn
	DLS_ID_COMMON  = "common"         // 乐高公共ID
	DLS_LNM_COMMON = "ilcomm"         // 贷款公共ID
	DLS_TYPE_CUS   = "CUS"            // 客户切片类型  //存款客户号
	DLS_TYPE_PHN   = "PHN"            // 手机切片类型
	DLS_TYPE_CMM   = "CMM"            // 乐高公共切片类型
	DLS_TYPE_CNT   = "CNT"            // 合同号切片类型
	DLS_TYPE_DBT   = "DBT"            // 借据号切片类型
	DLS_TYPE_LNM   = "LNM"            // 贷款切片类型
	DLS_TYPE_IDN   = "IDN"            // 证件信息切片类型

	DLS_TYPE_ACC   = "ACC"            // 存款账户切片类型
	DLS_TYPE_SCM   = "SCM"            // 存款公共切片类型
)
const (
	CU000006     = "CU000006"
)

const (
	ERRCODE_IL9  = "IL99000009"
	ERRCODE_IL10 = "IL99000010"
	ERRCODE_IL11 = "IL99000011"
	ERRCODE_IL12 = "IL99000012"
)